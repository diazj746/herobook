
# Custom ActionController for StrongParameters
# Devise workaround for Rails 4
# MUST be specified for devise to use in the config/routes.rb 
# Overwrite:
# devise_for :users
# With:
# devise_for :users, :controllers => { registrations: 'registrations' }
#
# The TreeBook tutorial application was originally developed on Rails 3
# using an attr_accessor (attr_accessible) to access custom user fields in the db
# This class enables the use of Rails 4 by customizing the "strong_parameters"
# by overwrititng the devise controller action
#
# Authors: Jaco Pretorious via Tanner Brandt
# Source: https://teamtreehouse.com/forum/attraccessible-missing-in-rails-4-heres-the-workaround-for-the-treebook-tutorial
#

class RegistrationsController < Devise::RegistrationsController

  #Routing for devise
  def after_sign_up_path_for(resource)
    '/feed'
  end

  private
 
  def sign_up_params
  	#Original controller used: devise_parameter_sanitizer.sanitize(:sign_up) 
    params.require(:user).permit(:first_name, :last_name, :profile_name, :email, :password, :password_confirmation)
  end
 
  def account_update_params
  	#Original controller used: devise_parameter_sanitizer.sanitize(:account_update)
    params.require(:user).permit(:first_name, :last_name, :profile_name, :email, :password, :password_confirmation, :current_password)
  end
end