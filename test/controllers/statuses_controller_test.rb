#Controllers(or functional) tests talk to the database and render the views
#The scope of this file is only controllers
require 'test_helper'

class StatusesControllerTest < ActionController::TestCase
  setup do
    @status = statuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:statuses)
  end

  test "should be redirected when not logged in" do
    get :new
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should render new page when logged in" do
    #devise helper sign_in
    sign_in users(:connan)
    get :new
    assert_response :success
  end

  test "should be logged in to post a status" do
    post :create, status: { conent: "Hello"}
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should create status when logged in" do
    sign_in users(:connan)
    assert_difference('Status.count') do
      post :create, status: { content: @status.content, user_id: @status.user_id, recipient_id: @status.user_id}
    end

    assert_redirected_to status_path(assigns(:status))
  end

  test "should show status" do
    get :show, id: @status
    assert_response :success
  end

  test "should get edit when logged in" do
    sign_in users(:connan)
    get :edit, id: @status, user_id: @status
    assert_response :success
  end

    test "should redirect statusupdate when not logged in" do
    patch :update, id: @status, user_id: @status, status: { content: @status.content}
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should update status when logged in" do
    sign_in users(:connan)
    patch :update, id: @status, user_id: @status, status: { content: @status.content}
    assert_redirected_to status_path(assigns(:status))
  end

  test "should destroy status" do
    assert_difference('Status.count', -1) do
      delete :destroy, id: @status
     end

    assert_redirected_to statuses_path
  end
end
