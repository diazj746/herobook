class Action < ActiveRecord::Base
	belongs_to :hero

	#Hero validation tests
	validates :hero_id, presence: true

	#There is no mandate for housing complex code in a Model-View-Controller (MVC) architectural pattern.
	#Here Models are typically responsible for persisting data from application to the database.
	#Controllers are typically responsible for receiving and request and returning a viable response.
	#Views draw the information.
	#It is possible that the functionality defined here should move to a module (lib) that classes can use
	#This first implementation will test this theory.

	#Constants capitalized
	LOC = ["Town", "Wilderness", "Dungeon"]

	#Shake a Leg is a class method
	def self.shakeALeg(hero)
		unless hero.nil?
			#New allows instantiation without saving
			action = Action.new
			action.hero_id = hero.id

			#picks new location
			case hero.location.downcase
			when LOC[0].downcase
				action.location = LOC[0]
				action.result = hero.name.to_s + " felt the call of the wild."
				hero.location = LOC[1]
			when LOC[1].downcase
				action.location = LOC[1]
				action.result = hero.name.to_s + " decided to be civil."
				hero.location = LOC[0]
			when LOC[2].downcase
				action.location = LOC[2]
				action.result = hero.name.to_s + " is done with rats."
				hero.location = LOC[1]
			else
				action.result = "Error: " + "unexpected location: ["+ hero.location.downcase + "] for " + hero.name.to_s + ", redirecting to " + LOC[0] + "!"
				action.location = hero.location = LOC[0]
			end

			return heroActionResult = [hero, action]
		end
	end

	#Look for Trouble
	def self.adventure(hero)
		unless hero.nil?
			#Capture variable in case it is changed
			startLocation = hero.location.downcase

			#New allows instantiation without saving
			action = Action.new
			action.hero_id = hero.id
			action.location = hero.location

			randNum = 1 + rand(100)

			#Dungeon:
			case startLocation
			#Town:
			when LOC[0].downcase
				#  100% No gold - kicked out
				if hero.gold == 0
					action.location = LOC[0]
					action.result = hero.name.to_s + " went to look for money."
					hero.location = LOC[1]
				else
					case randNum
					when 1 .. 10
					#1- 10% Brawl, gold down-, state +
						action.result = hero.name.to_s + " got in a brawl, dropped wallet!"
						hero.gold = hero.gold - 15
						hero.state = "Bruised and poor"
					#2- 10% Go on dates, gold down--, state +
					when 11 .. 20
						action.result = hero.name.to_s + " had a great date and picked up the tab."
						hero.gold = hero.gold - 15
						hero.state = "Feeling loved"
					#3- 10% Go on dates, gold down--, state -
					when 21 .. 30
						action.result = hero.name.to_s + " went on a hellish date and paid damages."
						hero.gold = hero.gold - 15
						hero.state = "Glad that's over with"
					#4- 10% Gamble, gold down--
					when 31 .. 40
						action.result = hero.name.to_s + " gambled and lost.."
						hero.gold = hero.gold - 50
						hero.state = "Almost had it"
					#5- 10% Gamble, gold up++
					when 41 .. 50
						action.result = hero.name.to_s + " gambled and won!"
						hero.gold = hero.gold + 50
						hero.state = "Super lucky"
					#6- 10% Pay bills gold down--
					when 51 .. 60
						action.result = hero.name.to_s + " paid some bills."
						hero.gold = hero.gold - 50
						hero.state = "Feeling ripped off"
					#7- 10% Listen to Bards, gold down-, state +
					when 61 .. 70
						action.result = hero.name.to_s + " listenned to some bards."
						hero.gold = hero.gold + 10
						hero.state = "Inspired"
					#8- 10% Catch up on Homework, state +
					when 71 .. 80
						action.result = hero.name.to_s + " wrote another entry on his ballad."
						hero.state = "Accomplished"
					#9- 20% Upgrade gear, gold --
					when 81 .. 100
						action.result = hero.name.to_s + " fixed broken gear."
						hero.gold = hero.gold - 50
						hero.state = "That was costly"
					#Error
					else
						action.location = LOC[0]
						action.result = "Error: " + "unexpected random value for " + hero.name.to_s + ", while in " + LOC[0] + "!"
					end
				end

			#Wilderness:
			when LOC[1].downcase
				case randNum
				#1- 5%  smell flowers, state +
				when 1 .. 5
					action.result = hero.name.to_s + " stops to smell the flowers."
					hero.state = "Happy."
				#2- 5%  find gear
				when 6 .. 10
					# This code should be a function
					gear = rand(2)
					newGear = "Nothing"
					if gear == 0
						hero.weapon = newGear = Action.gearUp(0)
					else
						hero.armor = newGear = Action.gearUp(2)
					end
					action.result = hero.name.to_s + " found and equiped " + newGear + "!"
					hero.state = "Wishing there was a mirror."
				#3- 15% fall in dungeon, state +
				when 11 .. 25
					action.result = hero.name.to_s + " found(fell into) dungeon!"
					hero.state = "Ahhhhhhhhhhh!"
					hero.location = LOC[2]
				#4- 15% fight wildlife, state ~
				when 26 .. 40
					action.result = hero.name.to_s + " fights the wildlife!"
					hero.state = "Glad he had his rabies shot."
				#5- 15% find treasure gold up+
				when 41 .. 55
					action.result = hero.name.to_s + " found a bag of money!"
					hero.gold = hero.gold + 15
					hero.state = "Feeling Lucky."
				#6- 15% camp, state+
				when 56 .. 60
					action.result = hero.name.to_s + " put up camp."
					hero.state = "Feeling rested."
				#7- 15% get lost, state-
				when 61 .. 85
					action.result = hero.name.to_s + " is lost."
					hero.state = "Can't see the forest for the trees."
				#8- 15% fight bandits gold  up+
				when 86 .. 100
					action.result = hero.name.to_s + " fought bandits and made some money!"
					hero.gold = hero.gold + 15
					hero.state = "Not too shabby."
				#Error
				else
					action.location = LOC[1]
					action.result = "Error: " + "unexpected random value for " + hero.name.to_s + ", while in " + LOC[1] + "!"
				end

			when LOC[2].downcase
				case randNum
				#1- 5%  fight boss monster level up, state +
				when 1 .. 5
					action.result = hero.name.to_s + " fought the boss monster. Instant Level UP!"
					hero.level = hero.level + 1
					hero.state = "Too cool for school"
				#2- 5%  find gear
				when 6 .. 10
					# Alternate Weapon, Armor using mod on action count
					gear = rand(2)
					newGear = "Nothing"
					if gear == 0
						hero.weapon = newGear = Action.gearUp(0)
					else
						hero.armor = newGear = Action.gearUp(2)
					end
					action.result = hero.name.to_s + " found and equiped " + newGear + "!"
					hero.state = "Hoping new gear is not cursed."
				#3- 10% find treasure gold up+++, state +
				when 11 .. 20
					action.result = hero.name.to_s + " found a box full of old sparklies. Gold ++ !"
					hero.gold = hero.gold + 50
					hero.state = "Thinking of what to buy."
				#4- 10% freak out and leave
				when 21 .. 30
					action.result = "This dank is freaking out " + hero.name.to_s + "! Time to leave."
					hero.state = "I'm not scared, I think my messenger pidgeon is cooing."
					hero.location = LOC[1]
				#5- 10% pass out loose gold, state -
				when 31 .. 40
					action.result = hero.name.to_s + " passes out. Looses gold!"
					hero.gold = hero.gold - 10
				#6- 10% dodge trap, state +
				when 41 .. 50
					action.result = "Trap almost gets the best of " + hero.name.to_s + "!"
					hero.state = "Who keeps putting down all these traps."
				#7- 15% fight minion gold up+ , state ~
				when 51 .. 65
					action.result = "Minion attacks " + hero.name.to_s + "! No problem, gold up."
					hero.state = "Feeling strong."
					hero.gold = hero.gold + 5
				#8- 15% fight creature gold up+, state ~
				when 66 .. 80
					action.result = hero.name.to_s + " fights creature!"
					hero.state = "Hoping whatever I just killed is not endangered."
				#9- 20% wander in darkness, state -
				when 81 .. 100
					action.result = hero.name.to_s + " wanders in the darkness! Not much to report."
					hero.state = "Bored, cold and damp."
				#Error
				else
					action.location = LOC[2]
					action.result = "Error: " + "unexpected random value for " + hero.name.to_s + ", while in " + LOC[2] + "!"
				end

			#Error
			else
				action.result = "Error: " + "unexpected location: ["+ hero.location.downcase + "] for " + hero.name.to_s + ", redirecting to " + LOC[0] + "!"
				action.location = hero.location = LOC[0]
			end

			#Check for level up!
			hero.level = Action.levelUp(hero)
			#Don't over spend
			if hero.gold < 0
				hero.gold = 0
			end

			#Done!
			return heroActionResult = [hero, action]
		end
	end

	#Level up
	def self.levelUp(hero)
		unless hero.nil?
			#Use the action count as xp
			actionCount = Action.where(hero_id: hero.id).count

			#Quadratic formula where it levels up at action count, 50, 100, 150
			targetLevel = (25 + Math.sqrt(625 + 100 * actionCount)) / 50
			targetLevel = targetLevel.floor

			#Level up
			if targetLevel > hero.level
				return targetLevel
			end

			return hero.level
		end

		return 0
	end

	#Gear up
	def self.gearUp(type)
		#Pick Random
		rand1 = rand(5)
		rand2 = rand(4)

		if type == 1
			style = [ 'Basic', 'Copper', 'Iron', 'Steel', 'Magic' ]
			type =  [ 'Stick', 'Knife', 'Sword', 'Lance' ]
		elsif type == 2
			style = [ 'Old', 'Leather', 'Iron', 'Steel', 'Magic' ]
			type =  [ 'Clothes', 'Robe', 'Mail', 'Armor' ]
		else
			style = "gearUp Error :"
			type = type.to_s
		end

		return style[rand1] + ' ' + type[rand2]
	end

end
