class HeroesController < ApplicationController
  before_action :set_hero, only: [:edit, :update, :destroy]
  before_action :authenticate_user!, only: [:new, :create, :edit, :update]

# GET /hero/1
# GET /hero/1.json
  def show
    #Create an instance variable which becomes available to views
    @hero =Hero.find_by_user_id(params[:id])

    if @hero
      @actions = @hero.actions.all.order(created_at: :desc)
      render action: :show
    else
      @hero = Hero.new
      render action: :new
    end
  end

  # GET /hero/new
  def new
    @hero = Hero.new
  end

  # GET /hero/1/edit
  def edit
    #Create an instance variable which becomes available to views
    @hero =Hero.find_by_id(params[:id])
    if @hero
      render action: :edit
    end
  end

  # POST /
  # POST /hero.json
  def create
    # Could be rewritten as @hero = @user.hero.new(create_hero_params) if necessary
    @hero = Hero.new(create_hero_params)

    respond_to do |format|
      if @hero.save
        format.html { redirect_to :action => 'show', :id => @hero.user_id, notice: 'Hero was successfully created.' }
        format.json { render :show, status: :created, location: @hero }
      else
        format.html { render :new }
        format.json { render json: @hero.errors, hero: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hero/1
  # PATCH/PUT /hero/1.json
  def update
    respond_to do |format|
      if @hero.update(update_hero_params)
        format.html { redirect_to :action => 'show', :id =>@hero.user_id, notice: 'Hero was successfully updated.' }
        format.json { render :show, status: :ok, location: @hero }
      else
        format.html { render :edit }
        format.json { render json: @hero.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /statuses/1
  # DELETE /statuses/1.json
  def destroy
    @hero.destroy
    respond_to do |format|
      format.html { redirect_to statuses_url, notice: 'Hero was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # Strong Params
  private

  # Use callbacks to share common setup or constraints between actions.
  def set_hero
    @hero = Hero.find(params[:id])
  end

  def create_hero_params
    params.require(:hero).permit(:user_id, :name, :location, :state, :weapon, :armor, :level, :gold)
  end

  def update_hero_params
    params.require(:hero).permit(:name)
  end

  def update_hero_stats
    params.require(:hero).permit(:location, :state, :weapon, :armor, :level, :gold)
  end

end
