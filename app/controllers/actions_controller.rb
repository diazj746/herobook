class ActionsController < ApplicationController

  def index
    #TODO: Should probably use the find_each(batch_size: 100) to optimize multiple records
    @actions = Action.all.order(created_at: :desc)
  end

    # GET /action/new
  def new
    @action = Action.new
  end

  def createAction()
    puts "** CREATING **"
    hero = Hero.find(params[:heroId])
    actionType = params[:actionType]
    heroActionPair = nil

    unless hero.nil?

      case actionType
      when 'move'
        #puts "** Move Switch **"
        heroActionPair = Action.shakeALeg(hero)
      when 'adventure'
        #something happens
        heroActionPair = Action.adventure(hero)
      end

      hero = heroActionPair[0]
      @action = heroActionPair[1]

      unless @action.nil?
        respond_to do |format|
          puts "** Attempting Save **"
          if @action.save && hero.save
            #TODO: Figure out how to pass the notice message to the application
            format.js {render action: 'index', controller: 'statuses',  notice: 'Action was successfully created'}
          else
            render file: 'public/500', status: 404, formats: [:html]
          end
        end
      end

    end

  end

  # Strong Params
 private
  # Use callbacks to share common setup or constraints between actions.
  def set_action
    @action = Action.find(params[:id])
  end

  def create_action_params
    params.require(:action).permit(:hero_id, :result, :location)
  end

  def update_action_stats
    params.require(:action).permit(:result, :location)
  end

end
