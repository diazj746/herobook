# README #

This repository contains "HeroBook" which is a Ruby on Rails application for a simple social media site. The application is based of the [TreeBook](http://teamtreehouse.com/library/build-a-simple-ruby-on-rails-application) rails tutorial created by TreeHouse.com as an introduction to Ruby on Rails. 

This project is my first venture into rails and one that I would like to make the foundation of graduate masters project. The original TreeHouse app is developed on Rails 3 therefore the first challenge is making the necessary changes to have it work on Rails 4.

When possible a live version will pushed on to [Heroku](https://www.heroku.com/) (hosting) for demonstrating functionality and features.

### Function ###

* Summary: HeroBook is a simple social networking site in which users create accounts for their Role Playing Characters and watch automatic updates about their adventures. Users are not given direct control of their Heroes. Instead they will make limited decisions on what their heroes should focus on for their next status update. Heroes can join or disband with other heroes in the same location to share status.

* Version 1.0

### Set up ###

* Setup:
    * You can get the rails installer from [railsinstaller.org](http://railsinstaller.org/en) which includes things like Ruby, Rails, Git and Bundler.
    * Code for this project was edited using the [Sublime](http://www.sublimetext.com/) IDE
* Database:
    * DB = [PostgressSQL 9.4](http://www.postgresql.org/) 
    * Gem = pg 0.18.0 pre20141117110243
* How to run tests (coming soon)
* Deployment instructions (coming soon)

### Bundled Gems ###
See HeroBook / Gemfile.lock

### Contribution guidelines ###

* When possible use [markdown](https://bitbucket.org/tutorials/markdowndemo) on this Read Me file.

* Writing tests:
    1. Always branch out if implementing new features. Don't shoulder the responsibility of an unpopular or catastrophic feature alone. Get repo owner approval before merging back in.
    2. When possible use a test driven development approach ( write the failing test before writing the code that helps it pass).
    3. All automated test must pass before submitting code.
    4. Don't rely on automated tests alone. Test the server version in your web browser for unusual behavior.

* Code review: When possible have someone look at your code. When in doubt use the following [review tips](http://www.codeproject.com/Articles/524235/Codeplusreviewplusguidelines)
    

### Contact ###

* Repo owner or admin: Jorge Diaz: jorge@jorgecomics.com