class AddRecipientUserIdToStatuses < ActiveRecord::Migration

  def change
    #:user_id will be automatic
    add_column :statuses, :recipient_id, :integer

    change_column :statuses, :user_id, :integer, :null => false
    change_column :actions, :hero_id, :integer, :null => false
    change_column :heroes, :user_id, :integer, :null => false

    #We don't know who created existing statuses
    #so fill the recipient with user_id
    say_with_time "Filling in :recipient_id!" do
      Status.find_each do |s|
        s.recipient_id = s.user_id
        s.save
      end
    end

    change_column :statuses, :recipient_id, :integer, :null => false

  end

end
