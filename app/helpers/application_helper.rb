module ApplicationHelper

	#Interprets flash type as string and returns string compatible with bootstrap styles
	def flash_class(type)
		case type
		when "alert"
			return "alert-error"
		when "notice"
			return "alert-success"
		else
			return ""
		end
	end

end
