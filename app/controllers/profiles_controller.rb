class ProfilesController < ApplicationController

  def show
    #Create an instance variable which becomes available to views and attempt to pass it a valid user based on form params
    @user = User.find_by_profile_name(params[:id])

    if @user

      @statuses = Status.all.where([("user_id = ? OR recipient_id = ?"),@user.id, @user.id]).order(created_at: :desc)
      render action: :show
    else
      #When a status request the error 404 html format rails will send the 404 page with not found status (404)
      render file: 'public/404', status: 404, formats: [:html]
    end
  end

end