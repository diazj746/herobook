# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# Uses jquery function($) with no arguments -- waiting for page to load
# However if the user navigates using the back browser button the page may
# not reload as expected and the functions may not be called

# In Coffee/jquery anything indented is part of the function
pageReady = ->
	# Runs when mouse hovers any tag of class "status"
	$('.status').hover (event) ->

		# used in debugging
		#console.log ("Hover Triggered")

		# Event calls a function that changes the class to .hover
		# which uses display in-line as opposed to none
		# see: statuses.css.scss
		$(this).toggleClass("hover")

# Ensure correct display functionality
$(document).ready(pageReady);
$(document).on('page:load', pageReady);