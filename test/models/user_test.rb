require 'test_helper'

class UserTest < ActiveSupport::TestCase

	test "a user should enter a profile name" do
		# Creates a new user
		user = User.new
		#Prevents saving
		assert !user.save
		assert !user.errors[:profile_name].empty?
	end

	test "a user should enter a first name" do
		user = User.new
		assert !user.save
		assert !user.errors[:first_name].empty?
	end

	test "a user should enter a last name" do
		user = User.new
		assert !user.save
		assert !user.errors[:last_name  ].empty?
	end

	test "a user should have a unique profile name" do
		user = User.new
		user.profile_name = users(:connan).profile_name
		assert !user.save
		#Print for errors before assert
		#puts user.errors.inspect
		assert !user.errors[:profile_name].empty?
	end

	test "a user should have a profile name without spaces" do
		user = User.new(first_name: 'Foo', last_name: 'Bar', email: 'FooBar@12345.com')
		user.password = user.password_confirmation = 'FooBarPassword1'
		user.profile_name = 'My Profile With Spaces'
		assert !user.save
		assert !user.errors[:profile_name].empty?
		assert user.errors[:profile_name].include?("Must be formated correctly.")
	end

	test "a user can have a correctly formatted profile name" do
		user = User.new(first_name: 'Foo', last_name: 'Bar', email: 'FooBar@12345.com')
		user.password = user.password_confirmation = 'FooBarPassword1'
		user.profile_name = 'Foo-Bar_1'
		assert user.valid?
	end

end
