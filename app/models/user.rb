class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  #Referencial Integrity
  has_many :statuses, dependent: :destroy

  #For simplicity we will only allow one hero with some referencial Integrity
  has_one :heroes, dependent: :destroy

  #The profile_name format must contian any "[]" letters, numbers, underscore, dashes more than once "+".
  validates :profile_name, presence: true,
                           uniqueness: true,
                           format: {
                              with: /\A[a-zA-Z0-9_-]+\z/,
                              message: 'Must be formated correctly.'
                           }
  validates :first_name, presence: true
  validates :last_name, presence: true


  def getProfile
    unless profile_name.to_s == ""
      profile_name
    else 
      "Error: profile_name not defined"
    end
  end

  def getHero (user_id)
    @usersHero = Hero.find_by_user_id(user_id)
    unless @usersHero.nil?
      @usersHero
    else
      nil
    end
  end

  def gravatar_url
    #Remove all spaces, lower case and hash
    hash = Digest::MD5.hexdigest(email.strip.downcase)
    "http://gravatar.com/avatar/#{hash}"
  end

end
