Rails.application.routes.draw do

 # To see routes while in development:
 # - use "rake routes" in command line
 # - or add "/rails/info/routes" to your local application path while running server
  get 'profiles/show'

  # attr_accessor is deprecated in Rails 4
  # devise_for :user
  # Tells devise to use our custom registration controller which has different "StrongParameters"
  devise_for :users, :controllers => { registrations: 'registrations' }

  # Allows us to set the feed path
  resources :statuses
    get 'allStatuses', to: 'statuses#index', as: :feed

  # Adds a new path 'register' and 'login' and a name route helper :register :login
  devise_scope :user do
    get 'register', to: 'devise/registrations#new', as: :register
    get 'login', to: 'devise/sessions#new', as: :login
    get 'logout', to: 'devise/sessions#destroy', as: :logout
  end

  # Root path for devise (user authentication)
  # Calls the 'index' action defined in the statuses_controller.rb
  root to: 'static_pages#about'

  # Pass the variable found for profile_name and use it in the params hash for the show action of profile controller
  get '/:id', to: 'profiles#show', as: 'showProfile'

  # Get
  resources :profiles
    get 'myStatuses/:id', to: 'profiles#show', as: 'myProfile'

  #TODO: Hide :id from URL. This is not trivial but there is a gem called friendly_id which has been recommended
  resources :heroes
    get 'myHero/:id', to: 'heroes#show', as: 'myHero'
    get 'createHero/:id', to: 'heroes#create', as: 'newHero'

  resources :actions
    post 'createAction/', to: 'actions#createAction'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
