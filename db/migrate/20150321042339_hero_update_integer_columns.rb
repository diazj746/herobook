class HeroUpdateIntegerColumns < ActiveRecord::Migration
  def change
    # Removing columns to avid issues with type conversion
    remove_column :heroes, :gold
    remove_column :heroes, :level

    # Setting the new type
    add_column :heroes, :level, :integer
    add_column :heroes, :gold, :integer
  end
end
