class UserStatusAssociations < ActiveRecord::Migration
  def change

  	# Remove the status.user_id before we do the association
  	remove_column :statuses, :user_id
  	
  	# Add the status.user_id associate reference user.id and index
  	change_table :statuses do |s|
  		s.belongs_to :user, index: true
  	end
  end
end
