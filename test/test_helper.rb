=begin
Test Guidelines

What to Include in your Functional Tests?
	You should test for things such as:

	was the web request successful?
	was the user redirected to the right page?
	was the user successfully authenticated?
	was the correct object stored in the response template?
	was the appropriate message displayed to the user in the view?

Available Request Types for Functional Tests
	There are 6 request types supported in Rails functional tests:
	get
	post
	patch
	put
	head
	delete

Available Hashes
After a request has been made using one of the 6 methods (get, post, etc.) and processed,
you will have 4 Hash objects ready for use:
	assigns - Any objects that are stored as instance variables in actions for use in views.
	cookies - Any cookies that are set.
	flash - Any objects living in the flash.
	session - Any object living in session variables.

	You can access the values by referencing the keys by string.
	You can also reference them by symbol name, except for assigns.
	For example:
		flash["gordon"]               flash[:gordon]
		session["shmession"]          session[:shmession]
		cookies["are_good_for_u"]     cookies[:are_good_for_u]

		# Because you can't use assigns[:something] for historical reasons:
		assigns["something"]          assigns(:something)
=end

ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...

  #Use the devise Test helpers
  class ActionController::TestCase
    include Devise::TestHelpers
  end

end
