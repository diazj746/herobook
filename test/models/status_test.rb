require 'test_helper'

class StatusTest < ActiveSupport::TestCase

  test "that a status requires content"  do
    status = Status.new
    assert !status.save
    assert !status.errors[:content].empty?, "Tried to save status content that was empty"
  end

  test "that a statu's content is at least 2 letters" do
    status = Status.new
    status.content = "H"
    assert !status.save
    assert !status.errors[:content].empty?, "Tried to save status content with less than 2 letters"
  end

  test "that a status has a user id" do
    status = Status.new
    status.content = "Hello"
    assert !status.save
    assert !status.errors[:user_id].empty?, "Ensure a status is associated with a user"
  end
end
