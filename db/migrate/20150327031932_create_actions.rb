class CreateActions < ActiveRecord::Migration
  def change
    create_table :actions do |t|
      t.string :result
      t.string :location

      # Coulds use polymorphic association for hero and group but
      # perhaps simple is better
      t.belongs_to :hero, index: true

      # Creates 2 fields: date and time
      t.timestamps
    end
  end
end
