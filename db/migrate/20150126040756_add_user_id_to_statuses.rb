class AddUserIdToStatuses < ActiveRecord::Migration
  def change
  	#Similar to SQL but with RUBY
  	#Command, target table, field, field type
  	add_column :statuses, :user_id, :integer
  	add_index :statuses, :user_id
  	remove_column :statuses, :name
  end
end
