class Hero < ActiveRecord::Base

	#DB relation definitions
	belongs_to :user

	#Referencial Integrity
	has_many :actions , dependent: :destroy

	#Hero validation tests
	validates :name, presence: true,
					length: { minimum: 2 }
	validates :user_id, presence: true

	#Hardcode some values by overwriting active model method
	def initialize(attributes = {}) #empty hash
		attr_with_defaults = {
			:weapon => 'Wooden Stick',
			:armor => "Old Clothes",
			:location => 'Town',
			:state => 'Very Optimistic',
			:gold => 0,
			:level => 1
			}.merge(attributes)
		super(attr_with_defaults)
	end

	def newAction
		@newAction = Action.new
	end

end
