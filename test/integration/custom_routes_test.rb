#This integration test will hit the entire application
require 'test_helper'

class CustomRoutesTest < ActionDispatch::IntegrationTest
  test "that /login route opens the login page" do
    get '/login'
    assert_response :success
  end

  #The site responds to a 302 re-direct response (temporary redirect)
  test "that /logout route opens the logout page" do
    get '/logout'
    assert_response :redirect
    assert_redirected_to '/'
  end

  test "that /register route opens the register page" do
    get '/register'
    assert_response :success
  end

  test "that a profile page works" do
    get '/Barbarian'
    assert_response :success
  end

end
