class Status < ActiveRecord::Base

	#DB relation definition
	belongs_to :user

  #TODO: Figure out correct belongs_to association using SQL Queries see profiles_controller

	#Used validation tests
	validates :content, presence: true,
						length: { minimum: 2 }
	validates :user_id, presence: true
  validates :recipient_id, presence: true

  def recipient(recipient_id)
    recipient = User.find_by_id(recipient_id)
    unless recipient.nil?
      recipient
    else
      nil
    end
  end

end
